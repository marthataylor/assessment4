#!/bin/bash
# Repeatable infrastructure script, things that may need to be run more than once
#RDS, Secrets, kubectl

#Command line arguments: need env, optional petclinic version (defaults to 1.0)
if (( $# < 1 ))
then
	echo "$0 <env> [<petclinic_version>=1.0]"
	exit 1
fi

ENV=$1
if [[ ! -z $2 ]]
then 
  version=$2
else
  echo "Deploying petclinic version 1.0"
  version=1.0
fi

#################################################################################

#Set variables from tfvars

bucket_name=$(cat ./makeVPC/${ENV}.tfvars | grep 'bucket_name' | awk -F'[=&]' '{print $2}' | tr -d '"')
ssh_key_name=$(cat ./makeVPC/${ENV}.tfvars | grep 'ssh_key_name' | awk -F'[=&]' '{print $2}' | tr -d '"')
chosen_region=$(cat ./makeVPC/${ENV}.tfvars | grep 'chosen_region' | awk -F'[=&]' '{print $2}' | tr -d '"')
ssh_key_path=$(cat ./makeVPC/${ENV}.tfvars | grep 'ssh_key_path' | awk -F'[=&]' '{print $2}' | tr -d '"')

#################################################################################

# Copy SSH key down from bucket (was deleted in initial script as this script may need to run independently of initial script)

aws s3 cp s3://$bucket_name/keys/${ssh_key_name}.pem ./$ssh_key_name.pem
chmod 600 ./$ssh_key_name.pem

#################################################################################

# Copy chosen database username and initial password to S3 bucket
dbuser=petclinic
dbpassword=petclinic
echo "$dbuser" >> "./DBUSER"
echo "$dbpassword" >> "./DBPASSWORD"
aws s3 cp ./DBUSER s3://$bucket_name/env:/${ENV}/secrets/DBUSER
aws s3 cp ./DBPASSWORD s3://$bucket_name/env:/${ENV}/secrets/DBPASSWORD
# Delete local copy
rm -f ./DBUSER
rm -f ./DBPASSWORD

#################################################################################

# Make namespace if namespace doesn't exist
cd ./kubectl 
if ! ./make_ns.sh $ENV $bucket_name $ssh_key_name
then 
  echo "error with namespace creation"
  exit 2
fi

#################################################################################

# Set initial password as Kubernetes secret
echo "Attempting to set K8 secrets for DB user and password"
cd ../Secrets
chmod +x ./database

if ./database $ENV $bucket_name $ssh_key_name
then 
  echo "Successfully set K8 secrets"
else 
  echo "Failed to set K8 secrets"
  exit 5
fi
#################################################################################

# Create RDS
cd ../rds
chmod +x ./get_secrets.sh
source ./get_secrets.sh $ENV

terraform init -backend-config="bucket=$bucket_name" -backend-config="region=$chosen_region" -backend-config="key=statefiles/rds.tfstate"
if terraform workspace select $ENV
then
  terraform apply -auto-approve -var-file=../makeVPC/$ENV.tfvars
else
  terraform workspace new $ENV
  terraform apply -auto-approve -var-file=../makeVPC/$ENV.tfvars
fi

unset TF_VAR_user TF_VAR_password

#################################################################################

# Re-set database secrets to more secure password
# In repeatable script, this will log onto Jenkins and run the script
# After this intitial setting of secrets, user can use Jenkins job to reset password

cd ../DBpasswords

# Get Jenkins DNS
aws s3 cp s3://$bucket_name/env:/dev/statefiles/Jenkins.tfstate jenkins.tfstate
Jenkins_dns=$(cat jenkins.tfstate | jq -r '.outputs.jenkins_dns.value')
rm jenkins.tfstate

# Create script to run DB config script on Jenkins
cat > runDBconfig.sh <<_END
   
   sudo chmod +x ./DatabaseConfig
   ./DatabaseConfig $ENV $bucket_name $ssh_key_name $chosen_region

_END

# Copy script to Jenkins
scp -o StrictHostKeyChecking=no -i ../$ssh_key_name.pem runDBconfig.sh ec2-user@$Jenkins_dns:~/.
scp -o StrictHostKeyChecking=no -i ../$ssh_key_name.pem DatabaseConfig ec2-user@$Jenkins_dns:~/.

# SSH onto Jenkins and run script
ssh -o StrictHostKeyChecking=no -i ../$ssh_key_name.pem ec2-user@$Jenkins_dns "sudo chmod +x ./runDBconfig.sh; ./runDBconfig.sh"

rm -f runDBconfig.sh


#################################################################################

# Make db connect service
cd ../kubectl
if ./db_service.sh $ENV $bucket_name $ssh_key_name
then 
  echo "DB connection service created"
else 
  echo "Failed to create connection to database"
  exit 3 
fi 

#################################################################################

# Make petclinic deployment
if ./molly-deploy.sh $ENV $bucket_name $ssh_key_name $version
then 
  echo "Petclinic deployed"
else 
  echo "Failed to deploy petclinic"
  exit 3 
fi 

#################################################################################

## Delete key from current directory (will remain on S3 bucket)
cd ../../
echo "SSH key will now be deleted from current directory, it will remain in S3 bucket"
rm -f $ssh_key_name.pem
echo "SSH key deleted"

