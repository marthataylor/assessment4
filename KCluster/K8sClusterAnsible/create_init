#!/bin/bash 
# Script to create the whole Kubernetes Cluster

# Export variables needed in ansible group_vars
AWS_DEFAULT_PROFILE=$profile
ANSIBLEENV="$PWD/environments/$env"
amiID=$(cat ../../makeVPC/dev.tfvars | grep -n5 image_id | grep $chosen_region | awk '{print $4}' | tr -d '"')
iam_profile=$(cat ../../makeVPC/dev.tfvars | grep -n s3_iam_profile | awk -F'[=&]' '{print $2}' | tr -d '"')
export PYTHON=$(which python)
export chosen_region ssh_key_name env amiID iam tags ANSIBLEENV bucket_name iam_profile

. ./vpc_vars # To find Terraform Output variables
export vpc_id vpc_security_groups ec2subnet lbsubnets ec2sg registrydns privreg

# Export Credentials
export AWS_ACCESS_KEY AWS_SECRET_KEY AWS_SESSION_TOKEN

ssh_key_path=../../$ssh_key_name.pem

# Check if the key file supplied exists
if [[ ! -e $ssh_key_path ]] || [[ -z $ssh_key_path ]]
then
	echo "SSH Key file $ssh_key_path does not exist, or not supplied" 1>&2
	exit 1
fi



if [[ $1 != SKIP ]]
then
	if [[ $1 == DEBUG ]]
	then
		ansible-playbook -vvvv -i $ANSIBLEENV --extra-vars "ansible_ssh_private_key_file=$ssh_key_path" create.yml
	else
		ansible-playbook -i $ANSIBLEENV --extra-vars "ansible_ssh_private_key_file=$ssh_key_path" create.yml
	fi
fi


if (( $? == 0 ))
then
		# Install Kubernetes on to the cluster and use the Public IP of the Master for access ip
		ssh -i $ssh_key_path ec2-user@$(grep -A1 '^\[controller' $ANSIBLEENV/hosts  | tail -1 | awk '{print $1}') "cd kubespray; ansible-playbook -i inventory/mycluster/inventory.ini --become --become-user=root cluster.yml"
		# Add a DNS LB to the cluster and DNS name with wild card for the apps
		if (( $? == 0 ))
		then
			ansible-playbook -i $ANSIBLEENV --extra-vars "ansible_ssh_private_key_file=$ssh_key_path" setup.yml
		fi
fi

# Copy hosts file to the S3 Bucket
aws s3 cp $ANSIBLEENV/hosts s3://mkmassessment4/env:/dev/
