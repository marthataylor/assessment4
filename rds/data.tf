data "terraform_remote_state" "infra" {
  backend = "s3"
  config = {
    bucket = "mkmassessment4"
    key    = "env:/dev/statefiles/makeVPC.tfstate"
    # key    = "statefiles/makeVPC.tfstate"
    region = var.chosen_region
  }
}

data "terraform_remote_state" "jenkins" {
  backend = "s3"
  config = {
    bucket = "mkmassessment4"
    key    = "env:/dev/statefiles/Jenkins.tfstate"
    region = var.chosen_region
  }
}
