#!/bin/bash

if (( $# < 1 ))
then
	echo "$0 <env>"
	exit 1
fi

bucket_name=$(cat ../makeVPC/$1.tfvars | grep bucket_name | awk -F'[=&]' '{print $2}' | tr -d '"')

aws s3 cp s3://$bucket_name/env:/$1/secrets/DBPASSWORD DBPASSWORD
aws s3 cp s3://$bucket_name/env:/$1/secrets/DBUSER DBUSER

TF_VAR_user=$(cat DBUSER)
TF_VAR_password=$(cat DBPASSWORD)

rm DBPASSWORD DBUSER
export TF_VAR_user
export TF_VAR_password
