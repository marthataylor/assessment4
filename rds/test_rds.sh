#!/bin/bash

if (( $# < 1 ))
then
	echo "$0 <env>"
	exit 1
fi
keypath=/Users/kimspijkersshaw/.ssh/kimVirginia.pem
source ../setenv.sh
source ./get_secrets.sh $1

# Make RDS to run tests on
terraform init
if ! terraform workspace select $1
then
  terraform workspace new $1
fi

if ! terraform plan -var-file=../makeVPC/dev.tfvars
then
    echo "Terraform plan failed"
    exit 1
fi 

if ! terraform apply -var-file=../makeVPC/dev.tfvars --auto-approve
then 
    echo "Terraform apply failed"
    exit 2
fi 

#Copy down tfvars
aws s3 cp s3://mkmassessment4/env:/dev/statefiles/rds.tfstate .
endpoint=$(cat rds.tfstate | jq -r '.outputs.rds_endpoint.value')

aws s3 cp s3://mkmassessment4/env:/dev/statefiles/bastion.tfstate .
bastdns=$(cat bastion.tfstate | jq -r '.outputs.bastion-lb-dns')

# Describe rds to check it is there
# aws rds describe-db-instances 

# SSH onto bastion and check for specific db entries
ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" \
   -i $keypath ec2-user@$bastdns 'mysql -h $endpoint -u $TF_VAR_user -p$TF_VAR_password'
