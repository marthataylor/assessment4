module "db" {
    source  = "terraform-aws-modules/rds/aws"
    version = "~> 2.0"

    identifier = "${var.project_name}-${var.env}-db"

    engine            = "mysql"
    engine_version    = "5.7.19"
    instance_class    = "db.t3.micro"
    allocated_storage = 5

    name     = "petclinic"
    username = var.user
    password = var.password
    port     = "3306"

#   iam_database_authentication_enabled = true

    vpc_security_group_ids = [data.terraform_remote_state.infra.outputs.sg_id]
    multi_az = true

    maintenance_window = "Mon:00:00-Mon:03:00"
    backup_window      = "03:00-06:00"

  # Enhanced Monitoring - see example for details on how to create the role
  # by yourself, in case you don't want to create it automatically
#   monitoring_interval = "30"
#   monitoring_role_name = "MyRDSMonitoringRole"
#   create_monitoring_role = true

    tags = {
            Name = "${var.tags}_RDS"
            Environment = var.env
            Project = var.project_name
            Info = "Database server for ${var.project_name} in ${var.env} environment"
            Owner = var.team
            Start_date = var.start_date
            End_date = var.end_date
            Team = var.team
           }

    # DB subnet group
    subnet_ids = [data.terraform_remote_state.infra.outputs.subnet_id_priv1, data.terraform_remote_state.infra.outputs.subnet_id_priv2, data.terraform_remote_state.infra.outputs.subnet_id_priv3]

    # DB parameter group
    family = "mysql5.7"

    # DB option group
    major_engine_version = "5.7"

    # Snapshot name upon DB deletion
    final_snapshot_identifier = "${var.project_name}-${var.env}-db"

    # Database Deletion Protection
    deletion_protection = false

    parameters = [
    {
        name = "character_set_client"
        value = "utf8"
    },
    {
        name = "character_set_server"
        value = "utf8"
    }
    ]

    options = [
    {
        option_name = "MARIADB_AUDIT_PLUGIN"

        option_settings = [
        {
            name  = "SERVER_AUDIT_EVENTS"
            value = "CONNECT"
        },
        {
            name  = "SERVER_AUDIT_FILE_ROTATIONS"
            value = "37"
        },
        ]
    },
    ]

    # provisioner "local-exec" {
    #     interpreter = []"/bin/bash"]
    #     command = <<_EOF
    #         #!/bin/bash
    #         mysql -h ${self.address} -u ${var.user} -p${var.password} < schema.sql
    #         mysql -h ${self.address} -u ${var.user} -p${var.password} < data.sql
    #     _EOF
    # }
}

resource "null_resource" "upload_schema" {
  depends_on = [module.db]
  provisioner "local-exec" {
    command = <<_EOF
        scp -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i ${var.ssh_key_path} *.sql ec2-user@${data.terraform_remote_state.jenkins.outputs.jenkins_dns}:
        ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i ${var.ssh_key_path} ec2-user@${data.terraform_remote_state.jenkins.outputs.jenkins_dns} '
            mysql -h ${module.db.this_db_instance_address} -u ${var.user} -p${var.password} < schema.sql;
            mysql -h ${module.db.this_db_instance_address} -u ${var.user} -p${var.password} < data.sql;'
    _EOF
  }
}
