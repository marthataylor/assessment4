variable "env" {
  type = string
}

variable "chosen_region" {
  type    = string
}

variable "tags" {
  type = string
}

variable "project_name" {
  type = string
}

variable "password" {
  type = string
}

variable "user" {
  type = string
}

variable "team" {
  type = string
}

variable "start_date" {
  type = string
}

variable "end_date" {
  type = string
}

variable "ssh_key_path" {
  type    = string
}