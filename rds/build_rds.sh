#!/bin/bash
if (( $# < 1 ))
then
	echo "$0 <env>"
	exit 1
fi

chosen_region=$(cat ../makeVPC/${1}.tfvars | grep 'chosen_region' | awk -F'[=&]' '{print $2}' | tr -d '"')
bucket_name=$(cat ../makeVPC/${1}.tfvars | grep 'bucket_name' | awk -F'[=&]' '{print $2}' | tr -d '"')
echo $bucket_name
echo $chosen_region
# source ../setenv.sh
source ./get_secrets.sh $1

terraform init -force-copy -backend-config="bucket=$bucket_name" -backend-config="region=$chosen_region" -backend-config="key=statefiles/rds.tfstate"
if terraform workspace select $1
then
  terraform apply -auto-approve -var-file=../makeVPC/${1}.tfvars
else 
  terraform workspace new $1
  terraform apply -auto-approve -var-file=../makeVPC/${1}.tfvars
fi
