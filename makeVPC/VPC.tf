
# Creating the VPC
resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags = {
    Name = "${var.tags}_VPC"
    Environment = var.env
    Project = var.project_name
    Info = "VPC for ${var.project_name}, ${var.env} environment"
    Owner = var.team
    Start_date = var.start_date
    End_date = var.end_date
    Team = var.team
  }
}

# Gateway for public subnet
resource "aws_internet_gateway" "default" {
  vpc_id = aws_vpc.vpc.id
}

resource "aws_eip" "eips" {
  vpc      = true
  count = 3
}

# Gateway for private subnets
resource "aws_nat_gateway" "ngwPrivate" {
  count = 3
  allocation_id = element(aws_eip.eips.*.id, count.index)
  subnet_id     = element([aws_subnet.private1.id,aws_subnet.private2.id,aws_subnet.private3.id], count.index)

  tags = {
    Name = "ngw-priv${var.tags}"
    Environment = var.env
    Project = var.project_name
    Info = "Nat gateway for ${var.project_name} in ${var.env} environment"
    Owner = var.team
    Start_date = var.start_date
    End_date = var.end_date
    Team = var.team
  }
}
