#General Settings
chosen_region="us-east-1"
profile="academy"

#Security Settings
ssh_key_path="../mkmassessment4.pem"
absolute_ssh_key_path="/Users/kimspijkers-shaw/Documents/assessment4/mkmassessment4.pem"
ssh_key_name="mkmassessment4"
bucket_name="mkmassessment4"
s3_iam_profile="S3FullAccessV2"
super_iam_profile="ALAcademyJenkinsSuperRole"

#Instance Settings
amiID="ami-0947d2ba12ee1ff75"
image_id={
    eu-west-1 = "ami-0bb3fad3c0286ebd5"
    us-west-2 = "ami-0528a5175983e7f28"
    us-east-1 = "ami-04bf6dcdc9ab498ca"
  }
# subnetID="subnet-762dd410"
instance_type="t2.micro"

#Tagging Settings
tags="mkmassessment4"
env="prod"
project_name="academyassessment4mkm"
team="martha-molly-kim"
start_date="2020-11-21"
end_date="2020-12-21"

#Network Settings
homeIP=["81.108.146.109/32", "109.148.23.91/32", "92.12.96.128/32"]
vpc_cidr="172.31.0.0/16"
pub1_cidr="172.31.0.0/19"
pub2_cidr="172.31.32.0/19"
pub3_cidr="172.31.64.0/19"
priv1_cidr="172.31.96.0/19"
priv2_cidr="172.31.128.0/19"
priv3_cidr="172.31.160.0/19"
dns_domain="academy.grads.al-labs.co.uk"
dns_zone_id="Z07626429N74Z31VDFLI"

#Database settings
db_name="petclinic"
db_user="petclinic"
db_password="petclinic"