# Creating a security group for LB and PC instances
resource "aws_security_group" "SG" {
  name        = "${var.project_name}-${var.env}-SG"
  description = "Security group for Load Balancers and Instances"

  vpc_id = aws_vpc.vpc.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = concat(var.homeIP, [var.vpc_cidr])
  }

  ingress {
    from_port   = 3200
    to_port     = 3200
    protocol    = "tcp"
    cidr_blocks = concat(var.homeIP, [var.vpc_cidr])
  }

  ingress {
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  ingress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = [var.vpc_cidr]
  }

  ingress {
    from_port   = 5000
    to_port     = 5000
    protocol    = "tcp"
    cidr_blocks = concat(var.homeIP, [var.vpc_cidr])
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.tags}_sg"
    Environment = var.env
    Project = var.project_name
    Info = "General security group for ${var.project_name} in ${var.env} environment"
    Owner = var.team
    Start_date = var.start_date
    End_date = var.end_date
    Team = var.team
  }
}

resource "aws_security_group" "kube-sg" {
  name        = "${var.project_name}-${var.env}-kube-sg"
  description = "Security group for Kubernetes cluster"

  vpc_id = aws_vpc.vpc.id

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = concat(var.homeIP, [var.vpc_cidr])
  }

    ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = -1
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.tags}_kube_sg"
    Environment = var.env
    Project = var.project_name
    Info = "Kubernetes security group for ${var.project_name} in ${var.env} environment"
    Owner = var.team
    Start_date = var.start_date
    End_date = var.end_date
    Team = var.team
  }
}
