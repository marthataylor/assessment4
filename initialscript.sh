#!/bin/bash

### Initial script to create Docker registry, launch VPC (with Jenkins), and build K8 cluster ###
# Parts of the infrastructure that only need to run once
#################################################################################

## Set-up of variables ##

# Check that correct input arguments are provided
ENV=$1

if [[ -z $ENV ]]
then
  echo "Must provide environment dev or prod as first argument"
  exit 1
fi



# Get and export required parameters: region, profile, SSH key name, bucket name, ami ID, subnet ID, tag name
profile=$(cat ./makeVPC/${ENV}.tfvars | grep 'profile' | awk -F'[=&]' '{print $2}' | tr -d '"')
chosen_region=$(cat ./makeVPC/${ENV}.tfvars | grep 'chosen_region' | awk -F'[=&]' '{print $2}' | tr -d '"')
bucket_name=$(cat ./makeVPC/${ENV}.tfvars | grep 'bucket_name' | awk -F'[=&]' '{print $2}' | tr -d '"')
amiID=$(cat ./makeVPC/${ENV}.tfvars | grep 'amiID' | awk -F'[=&]' '{print $2}' | tr -d '"')
subnetID=$(cat ./makeVPC/${ENV}.tfvars | grep 'subnetID' | awk -F'[=&]' '{print $2}' | tr -d '"')
env=$(cat ./makeVPC/${ENV}.tfvars | grep 'env' | awk -F'[=&]' '{print $2}' | tr -d '"')
ssh_key_name=$(cat ./makeVPC/${ENV}.tfvars | grep 'ssh_key_name' | awk -F'[=&]' '{print $2}' | tr -d '"')
ssh_key_path=$(cat ./makeVPC/${ENV}.tfvars | grep 'ssh_key_path' | awk -F'[=&]' '{print $2}' | tr -d '"')
tags=$(cat ./makeVPC/${ENV}.tfvars | grep 'tags' | awk -F'[=&]' '{print $2}' | tr -d '"')
dbuser=$(cat ./makeVPC/${ENV}.tfvars | grep 'dbuser' | awk -F'[=&]' '{print $2}' | tr -d '"')
dns_domain=$(cat ./makeVPC/${ENV}.tfvars | grep 'dns_domain' | awk -F'[=&]' '{print $2}' | tr -d '"')

# Export variable grabbed from .tfvars file
export profile chosen_region bucket_name amiID subnetID env ssh_key_name tags ssh_key_path ENV dns_domain

#If argument not given raise error
if [[ -z $chosen_region ]] || [[ -z $profile ]] || [[ -z $bucket_name ]] || [[ -z $amiID ]] \
|| [[ -z $subnetID ]] || [[ -z $tags ]] || [[ -z $ssh_key_name ]] || [[ -z $env ]] || [[ -z $ssh_key_path ]] #|| [[ -z $dbuser ]] || [[ -z $dns_domain ]]
then
  echo "Insufficient parameters in .tfvars file, please give: region, profile, SSH key name, bucket name, ami ID, subnet ID, tag name, environment"
  exit 1
fi

# Export default region
AWS_DEFAULT_REGION=$chosen_region
export AWS_DEFAULT_REGION


# Run the setenv script to export downloaded credentials
source ./setenv $profile


#Create S3 bucket with name given by $bucket_name
aws s3api create-bucket --acl private --bucket $bucket_name --create-bucket-configuration LocationConstraint=$chosen_region >/dev/null 2>&1

if aws s3api list-buckets --query "Buckets[].Name" | grep "$bucket_name"
then
  echo "S3 bucket $bucket_name successfully created"
else
  echo "Failed to create S3 bucket"
  exit 1
fi

# Block public access to the bucket
aws s3api put-public-access-block \
    --bucket $bucket_name \
    --public-access-block-configuration "BlockPublicAcls=true,\
    IgnorePublicAcls=true,BlockPublicPolicy=true,RestrictPublicBuckets=true"


# Check if provided key in .tfvars file is valid
echo -e "Checking if provided SSH key is valid"
checkSSHkey=$(aws ec2 describe-key-pairs --key-name $ssh_key_name --region $chosen_region | jq -r '.KeyPairs[].KeyName' 2>/dev/null)
if [[ -z $checkSSHkey ]]
then
  # If invalid, generate a new key and store in bucket
  echo -e "Provided SSH key is invalid"
  echo -e "Creating a new SSH key"
  aws ec2 create-key-pair --key-name $ssh_key_name | jq -r '.KeyMaterial' >$ssh_key_name.pem
  chmod 600 $ssh_key_name.pem
  aws s3 cp ./$ssh_key_name.pem s3://$bucket_name/keys/${ssh_key_name}.pem
else
  # If key is valid and on the local machine, use this key
  echo -e "Provided SSH key is valid"
  chmod 600 ./$ssh_key_name.pem
  aws s3 cp ./$ssh_key_name.pem s3://$bucket_name/keys/${ssh_key_name}.pem
  
fi



# Copy chosen database username and initial password to S3 bucket

dbpassword=petclinic
echo -n "$dbuser" >> "./DBUSER"
echo -n "$dbpassword" >> "./DBPASSWORD"
aws s3 cp ./DBUSER s3://$bucket_name/env:/${ENV}/secrets/DBUSER
aws s3 cp ./DBPASSWORD s3://$bucket_name/env:/${ENV}/secrets/DBPASSWORD
# Delete local copy
rm -f ./DBUSER
rm -f ./DBPASSWORD


#################################################################################

## Create VPC that everything else is inside ##

 cd makeVPC
 chmod +x create_vpc.sh 
if  ./create_vpc.sh 
then 
  echo "VPC created successfully"
else 
  echo "VPC failed to create" 
  exit 1
fi

#################################################################################

## Create Docker registry ##

cd ../makeRegistry
chmod +x create_registry.sh

if ./create_registry.sh 
then 
  echo "Registry created successfully"
else 
  echo "Registry failed to create" 
  exit 2
fi
jenkins_dns=$(terraform output jenkins_dns)
#################################################################################

# Make bastion
cd ../bastion_lb
chmod +x create_bastion_lb.sh
if ./create_bastion_lb.sh 
then 
  echo "Bastion created successfully"
else 
  echo "Bastion failed to create" 
  exit 4
fi

#################################################################################

## Create Jenkins Instance ##

# Run terraform within Jenkins directory - will create instance and run ansible playbook to provision
cd ../Jenkins
chmod +x create_Jenkins.sh
if ./create_Jenkins.sh 
then 
  echo "Jenkins created successfully"
else 
  echo "Jenkins failed to create" 
  exit 3
fi
jenkins_dns=$(terraform output jenkins_dns)


################################################################################

## Create initial petclinic image and send it to the registry ##
cd ../DockerImages
if ./remote_image_build.sh $bucket_name $ssh_key_name $jenkins_dns
then 
  echo "Petclinic image made and pushed to registry"
else 
  echo petclinic build failed 
  exit 5
fi

unset TF_VAR_user TF_VAR_password

################################################################################

## Create Kubernetes Cluster ##

cd ../KCluster/K8sClusterAnsible
chmod +x create_init
if ./create_init DEBUG
then 
  echo "Kubernetes cluster created"
else 
  echo "Creation of Kubernetes cluster failed"
  exit 5
fi
#################################################################################

## Delete key from current directory (will remain on S3 bucket)
cd ../../
echo "SSH key will now be deleted from current directory, it will remain in S3 bucket"
rm -f $ssh_key_name.pem
echo "SSH key deleted"

################################################################################
# Call repeatable.sh script to build RDS and secrets
echo "Will now run repeatable script"
chmod +x repeatable.sh
./repeatable.sh $ENV $bucket_name $ssh_key_name $chosen_region
