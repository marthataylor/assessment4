terraform {
  backend "s3" {
    key     = "statefiles/bastion.tfstate"
  }
}