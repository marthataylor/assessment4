variable "chosen_region" {
  type    = string
}

variable "bucket_name" {
  type    = string
}


variable "instance_type" {
  type    = string
}

variable "ssh_key_name" {
  type    = string
}

variable "env" {
  type = string
}

variable "image_id" {
  type = map
}

variable "dns_zone_id" {
  type    = string
}

variable "tags" {
  type = string
}

variable "project_name" {
  type = string
}

variable "dns_domain" {
  type = string
}

variable "team" {
  type = string
}

variable "start_date" {
  type = string
}

variable "end_date" {
  type = string
}