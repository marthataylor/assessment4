#Load balancer for Bastion
resource "aws_elb" "bastion_lb" {
  name            = "MKM-LB-Bastion-${var.env}"
  security_groups = [data.terraform_remote_state.infra.outputs.sg_id]
  subnets         = [data.terraform_remote_state.infra.outputs.subnet_id_pub1, data.terraform_remote_state.infra.outputs.subnet_id_pub2, data.terraform_remote_state.infra.outputs.subnet_id_pub3]

  listener {
    instance_port     = 22
    instance_protocol = "tcp"
    lb_port           = 22
    lb_protocol       = "tcp"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "TCP:22"
    interval            = 30
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 120
  connection_draining         = true
  connection_draining_timeout = 120

  tags = {
    Name = "${var.tags}_bastion-lb"
    Environment = var.env
    Project = var.project_name
    Info = "Bastion load balancer for ${var.project_name} in ${var.env} environment"
    Owner = var.team
    Start_date = var.start_date
    End_date = var.end_date
    Team = var.team
  }
}

#Launch Configuration for Bastion
resource "aws_launch_configuration" "laconf_bastion" {
  name                 = "MKM-LaunchConfig-Bastion-${var.env}"
  image_id             = var.image_id[var.chosen_region]
  instance_type        = var.instance_type
  iam_instance_profile = "S3AccessProfile"
  key_name             = var.ssh_key_name
  security_groups      = [data.terraform_remote_state.infra.outputs.sg_id]
  user_data              = <<EOF
        #!/bin/bash
        yum -y install mysql
        cat > /usr/bin/connect <<- '_END_'
        #!/bin/bash
        PS3="Please choose the service you would like to connect to: "
        select service in PetClinic Database
        do
        case $service in
            PetClinic)
                aws s3 cp s3://khc-ass3v2/keypairs/user_khc_${var.env}.pem ~/.ssh/user_khc_${var.env}.pem
                chmod 600 ~/.ssh/user_khc_${var.env}.pem
                echo "Connecting to $service... "
                sleep 2
                ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i ~/.ssh/user_khc_${var.env}.pem ec2-user@$petclinic-khc.academy.grads.al-labs.co.uk
                rm ~/.ssh/user_khc_${var.env}.pem
                ;;
            Database)
                echo "Connecting to $service... "
                sleep 2
                mysql -h khc-rds.cbwwctw553uk.us-west-2.rds.amazonaws.com -u petclinic -pcloudvet
                ;;
            *)
                echo "Invalid Option. Please try again."
                break
                ;;
        esac
        done
_END_
        chmod +x /usr/bin/connect
  EOF

  lifecycle {
    create_before_destroy = true
  }
}

#ASG for Bastion
resource "aws_autoscaling_group" "asg_bastion" {
  name                      = "mkm-asg-bastion-${var.env}"
  max_size                  = 3
  min_size                  = 1
  health_check_grace_period = 300
  health_check_type         = "ELB"
  desired_capacity          = 1
  force_delete              = true
  launch_configuration      = aws_launch_configuration.laconf_bastion.name
  vpc_zone_identifier       = [data.terraform_remote_state.infra.outputs.subnet_id_pub1, data.terraform_remote_state.infra.outputs.subnet_id_pub2, data.terraform_remote_state.infra.outputs.subnet_id_pub3]
  load_balancers            = [aws_elb.bastion_lb.name]

  tags = [
    {
    key                 = "Name"
    value               = "${var.tags}-${var.env}-bastion"
    propagate_at_launch = true
    },
    {
    key                 = "Environment"
    value               = var.env
    propagate_at_launch = true
    },
    {
    key                 = "Project"
    value               = var.project_name
    propagate_at_launch = true
    },
    {
    key                 = "Start_date"
    value               = var.start_date
    propagate_at_launch = true
    },
    {
    key                 = "End_date"
    value               = var.end_date
    propagate_at_launch = true
    },
    {
    key                 = "Info"
    value               = "Autoscaling group for bastion in ${var.env} environment for ${var.project_name}"
    propagate_at_launch = true
    },
    {
    key                 = "Owner"
    value               = var.team
    propagate_at_launch = true
    },
    {
    key                 = "Team"
    value               = var.team
    propagate_at_launch = true
    }]

  timeouts {
    delete = "15m"
  }

}

#ASG Attachment for Bastion
resource "aws_autoscaling_attachment" "asg_attachment_bastion" {
  autoscaling_group_name = aws_autoscaling_group.asg_bastion.id
  elb                    = aws_elb.bastion_lb.id
}

#Route53 for Bastion - redirects ssh to the bastion instance
resource "aws_route53_record" "www-bastion" {
  zone_id = var.dns_zone_id
  name    = "bastion.${var.env}.${var.tags}.${var.dns_domain}"
  type    = "CNAME"
  ttl     = "60"
  records = [aws_elb.bastion_lb.dns_name]
}
