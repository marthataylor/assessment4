#!/bin/bash

ENV=$1

if [[ -z $ENV ]]
then
  echo "Not given or invalid parameter ENVIRONMENT (dev or prod)"
  exit 1
fi

chosen_region=$(cat ../makeVPC/${ENV}.tfvars | grep 'chosen_region' | awk -F'[=&]' '{print $2}' | tr -d '"')
bucket_name=$(cat ../makeVPC/${ENV}.tfvars | grep 'bucket_name' | awk -F'[=&]' '{print $2}' | tr -d '"')
profile=$(cat ../makeVPC/${ENV}.tfvars | grep 'profile' | awk -F'[=&]' '{print $2}' | tr -d '"')
export chosen_region bucket_name profile

source ../setenv $profile
terraform init -force-copy -backend-config="bucket=$bucket_name" -backend-config="region=$chosen_region" -backend-config="key=statefiles/bastion.tfstate"
terraform workspace select $ENV
terraform destroy -auto-approve -var-file="../makeVPC/${ENV}.tfvars"
