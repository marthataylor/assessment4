data "terraform_remote_state" "infra" {
  backend = "s3"
  config = {
    bucket = var.bucket_name
    key    = "env:/${var.env}/statefiles/makeVPC.tfstate"
    # key    = "statefiles/makeVPC.tfstate"
    region = var.chosen_region
  }
}
