# Get data from makeVPC directory
data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket = var.bucket_name
    key    = "env:/${var.env}/statefiles/makeVPC.tfstate"
    region = var.chosen_region
  }
}
