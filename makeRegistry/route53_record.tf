resource "aws_route53_record" "Registry_r53" {
  zone_id = var.dns_zone_id
  name    = "Registry.${var.env}.${var.tags}.${var.dns_domain}"
  type    = "CNAME"
  ttl     = "60"
  records = [aws_instance.registry.public_ip]
}
