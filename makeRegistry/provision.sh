#!/bin/bash -xv

# Install Docker
sudo yum -y update
sudo yum -y install docker
sudo systemctl start docker
sudo systemctl enable docker
#sudo usermod -G docker ec2-user

# Run registry
sudo docker run -d -p 5000:5000 --restart=always --name registry -it -v /var/lib/dockerreg:/var/lib/registry registry:2.7.1
