
# Create registry instance
resource "aws_instance" "registry" {
  #depends_on = [template_file.user-data-registry]
  ami           = var.image_id[var.chosen_region]
  instance_type = var.instance_type
  key_name      = var.ssh_key_name
  vpc_security_group_ids = [data.terraform_remote_state.vpc.outputs.sg_id]
  subnet_id              = data.terraform_remote_state.vpc.outputs.subnet_id_pub1
  #user_data = data.template_file.user-data-registry.rendered

  tags = {
    Name = "registry-${var.tags}-${var.env}"
    Environment = var.env
    Project = var.project_name
    Info = "Docker registry for ${var.project_name} in ${var.env} environment"
    Owner = var.team
    Start_date = var.start_date
    End_date = var.end_date
    Team = var.team
  }
}

# Sleep for a minute to allow registry instance to start
resource "null_resource" "delay" {
 depends_on = [aws_instance.registry]
 provisioner "local-exec" {
   command = "sleep 60"
 }
}

# Provision the registry using provision.sh
resource "null_resource" "provision_registry" {
  depends_on = [null_resource.delay]
  provisioner "local-exec" {
    command = <<_EOF
         scp -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i ${var.ssh_key_path} provision.sh ec2-user@${aws_instance.registry.public_dns}:
         ssh -o "StrictHostKeyChecking=no" -o "UserKnownHostsFile=/dev/null" -i ${var.ssh_key_path} ec2-user@${aws_instance.registry.public_dns} ' sudo chmod +x provision.sh ;
             ./provision.sh '
_EOF
  }
}
