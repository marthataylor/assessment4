output "registry_dns" {
  value       = aws_instance.registry.public_dns
  description = "The dns name of the registry"
}

output "route_53_registry_dns" {
  value       = aws_route53_record.Registry_r53.name
  description = "The Route 53 dns name of the registry"
}

output "registry_private_ip" {
  value       = aws_instance.registry.private_ip
  description = "The private ip of the registry"
}
