resource "aws_route53_record" "Jenkins_r53" {
  zone_id = var.dns_zone_id
  name    = "Jenkins.${var.env}.${var.tags}.${var.dns_domain}"
  type    = "CNAME"
  ttl     = "60"
  records = [aws_instance.Jenkins.public_dns]
  allow_overwrite = true
}
