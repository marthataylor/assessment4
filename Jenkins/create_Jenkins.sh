 
 # Script to make Jenkins
 terraform init -force-copy -backend-config="bucket=$bucket_name" -backend-config="region=$chosen_region" -backend-config="key=statefiles/Jenkins.tfstate" 
 if terraform workspace select $ENV
 then
   terraform apply -auto-approve -var-file=../makeVPC/$ENV.tfvars
 else
   terraform workspace new $ENV
   terraform apply -auto-approve -var-file=../makeVPC/$ENV.tfvars
 fi