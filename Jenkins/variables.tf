
variable "chosen_region" {
  type    = string
}

variable "env" {
  type    = string
}

variable "project_name" {
  type    = string
}

variable "s3_iam_profile" {
  type    = string
}

variable "super_iam_profile" {
  type    = string
}

variable "homeIP" {
  type    = list(string)
}

variable "vpc_cidr" {
  type    = string
}

variable "pub1_cidr" {
  type    = string
}

variable "pub2_cidr" {
  type    = string
}

variable "pub3_cidr" {
  type    = string
}

variable "priv1_cidr" {
  type    = string
}

variable "priv2_cidr" {
  type    = string
}

variable "priv3_cidr" {
  type    = string
}

variable "instance_type" {
  type    = string
  default = "t2.micro"
}

variable "bucket_name" {
  type = string
}

variable "tags" {
  type    = string
}

variable "ssh_key_name" {
  type    = string
}

variable "ssh_key_path" {
  type    = string
}

variable "absolute_ssh_key_path" {
  type    = string
}

variable "dns_zone_id" {
  type    = string
}

variable "dns_domain" {
  type    = string
}

variable "image_id" {
  type    = map
}
