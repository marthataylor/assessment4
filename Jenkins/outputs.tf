output "jenkins_dns" {
  value       = aws_instance.Jenkins.public_dns
  description = "The dns name of the jenkins"
}

output "route_53_jenkins_dns" {
  value       = aws_route53_record.Jenkins_r53.name
  description = "The Route 53 dns name of the jenkins"
}