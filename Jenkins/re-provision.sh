#!/bin/bash
if (( $# < 1 ))
then
	echo "$0 <env> "
	exit 1
fi
ENV=$1
export bucket_name=$(cat ../makeVPC/${ENV}.tfvars | grep 'bucket_name' | awk -F'[=&]' '{print $2}' | tr -d '"')
absolute_ssh_key_path=/Users/kimspijkers-shaw/Documents/assessment4/mkmassessment4.pem
ansible-playbook -i ./environments/${ENV} site.yml --extra-vars "key_path=${absolute_ssh_key_path}, registry_dns=https://ec2-3-237-187-67.compute-1.amazonaws.com"