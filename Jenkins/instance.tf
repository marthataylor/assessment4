
# Create Jenkins instance
resource "aws_instance" "Jenkins" {
  #depends_on = [template_file.user-data-registry]
  ami           = var.image_id[var.chosen_region]
  instance_type = "t3a.large"
  key_name      = var.ssh_key_name
  vpc_security_group_ids = [data.terraform_remote_state.vpc.outputs.sg_id]
  subnet_id              = data.terraform_remote_state.vpc.outputs.subnet_id_pub1
  #user_data = data.template_file.user-data-registry.rendered
  iam_instance_profile   = var.super_iam_profile
  tags = {
    Name = "Jenkins-${var.tags}-${var.env}"
  }
}



# Sleep for a minute to allow Jenkins instance to start
resource "null_resource" "Jenkinsdelay" {
 depends_on = [aws_instance.Jenkins]
 provisioner "local-exec" {
   command = "sleep 60"
 }
}



# Add Jenkins private DNS to ansible inventory/hosts file
resource "null_resource" "JenkinsDNSToAnsibleHosts" {
  depends_on = [null_resource.Jenkinsdelay]
  provisioner "local-exec" {
     command = "echo '[jenkins]\n${aws_instance.Jenkins.public_dns}' > ./environments/${var.env}/hosts"
   }
}


# Add bastion  DNS to ansible group_vars
# resource "null_resource" "BastionDNSToAnsibleVars" {
  # depends_on = [null_resource.Jenkinsdelay]
  # provisioner "local-exec" {
    #  command = <<_EOF
      #echo ansible_ssh_common_args: ""-o ProxyCommand='ssh -W %h:%p ec2-user@${data.terraform_remote_state.bastion.outputs.bastion-lb-dns}'"" >> ./environments/${var.env}/group_vars/all
#_EOF
  #  }
# }

 # Sleep for a minute to allow copying across of variables
 # resource "null_resource" "Jenkinsdelay2" {
 #  depends_on = [null_resource.JenkinsDNSToAnsibleHosts]
 #  provisioner "local-exec" {
 #    command = "sleep 60"
 #  }
 # }

# Provision Jenkins instance through bastion by calling ansible playbook
resource "null_resource" "provision_Jenkins" {
  depends_on = [null_resource.JenkinsDNSToAnsibleHosts]
  provisioner "local-exec" {
    command = "ansible-playbook -i ./environments/${var.env} site.yml --extra-vars 'key_path=${var.absolute_ssh_key_path} registry_dns=${data.terraform_remote_state.registry.outputs.registry_dns} bucket_name=${var.bucket_name}'"
  }
}
