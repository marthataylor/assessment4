# Get data from makeVPC directory
data "terraform_remote_state" "vpc" {
  backend = "s3"
  config = {
    bucket = var.bucket_name
    key    = "env:/${var.env}/statefiles/makeVPC.tfstate"
    region = var.chosen_region
  }
}

# Get data from bastion directory
data "terraform_remote_state" "bastion" {
  backend = "s3"
  config = {
    bucket = var.bucket_name
    key    = "env:/${var.env}/statefiles/bastion.tfstate"
    region = var.chosen_region
  }
}

# Get data from registry directory
data "terraform_remote_state" "registry" {
  backend = "s3"
  config = {
    bucket = var.bucket_name
    key    = "env:/${var.env}/statefiles/makeRegistry.tfstate"
    region = var.chosen_region
  }
}
