#!/bin/bash
bucket_name=$1
ssh_key_name=$2
jenkins_dns=$3

echo "Getting Jenkins DNS"
if [[ -z $jenkins_dns ]]
then 
    aws s3 cp s3://$bucket_name/env:/dev/statefiles/Jenkins.tfstate jenkins.tfstate
    jenkins_dns=$(cat jenkins.tfstate | jq -r '.outputs.jenkins_dns.value')
    rm jenkins.tfstate
fi 

echo $jenkins_dns
echo "Creating image push script"
cat > petclinic/image_push.sh <<_END
    if ./compose_test.sh
    then 
        if ./send_to_registry.sh $bucket_name
        then 
            echo "image pushed to registry"
        else
            echo "Image push failed"
            exit 2
        fi
    else
        echo "Petclinic build failed"
        exit 1
    fi
_END

echo "Copying petclinic dir to Jenkins"
scp -o StrictHostKeyChecking=no -i ../$ssh_key_name.pem  -r petclinic ec2-user@$jenkins_dns:
echo "Running image push script"
ssh -o StrictHostKeyChecking=no -i ../$ssh_key_name.pem  ec2-user@$jenkins_dns "cd petclinic; chmod +x image_push.sh; ./image_push.sh"

rm petclinic/image_push.sh