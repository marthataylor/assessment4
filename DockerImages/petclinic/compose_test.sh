if ! docker-compose up -d --build
then
    echo "Petclinic build failed"
    exit 1
fi

count=1
while (( count < 6 ))
do
    if docker-compose logs petclinic | grep Started >/dev/null
    then
        echo "petclinic started"
        break
    else 
        echo "Petclinic start attempt $count failed .. retrying"
    fi
    sleep 30
    (( count = count + 1 ))
done

if (( count < 6 ))
then 
    count=1
fi

while (( count < 6 ))
do
    if curl -s localhost:8081 | grep petclinic >/dev/null
    then
        echo "successful build"
        break
    else 
        echo "Connection attempt $count failed .. retrying"
    fi
    sleep 30
    (( count = count + 1 ))
done

docker-compose down

if (( count >= 6 ))
then
    echo "Petclinic build failed"
    exit 1
fi