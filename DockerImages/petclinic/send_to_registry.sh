#!/bin/bash
#Pushes a petclinic docker image to the registry
#Requires bucket_name to be set as an environment variable or an argument
[[ -z $bucket_name ]] && bucket_name=$1

# Get registry dns
if aws s3 cp s3://$bucket_name/env:/dev/statefiles/makeRegistry.tfstate . >/dev/null
then
    regdns=$(cat makeRegistry.tfstate | jq -r '.outputs.registry_dns.value')
    echo $regdns
    rm makeRegistry.tfstate
    if [[ -z $regdns ]]
    then 
        echo "Private registry server not found"
        exit 4
    fi 
else
    echo "Cannot access registry state file"
    rm makeRegistry.tfstate
    exit 3
fi

# Look at things currently on registry to determine version number
if curl -s $regdns:5000/v2/pc/tags/list | jq -r '.tags[]' 2>&1 >/dev/null
then
    newest_tag=$(curl -s $regdns:5000/v2/pc/tags/list | jq -r '.tags[]' | sort | tail -n1)
    VERSION=$( echo "" | awk "{print $newest_tag  + 0.1}" )
else 
    VERSION=1.0
fi

# tag images imagename= "pc"
docker tag pc:latest $regdns:5000/pc:${VERSION}

#Push to private registry
if ! docker push $regdns:5000/pc:${VERSION} >/dev/null
then
    echo "Failed to push image to registry"
    exit 7
fi 

if curl -s $regdns:5000/v2/pc/tags/list | grep $VERSION
then
    echo "Image successfully pushed to registry"
else 
    echo "Failed to push image to registry"
    exit 7
fi

