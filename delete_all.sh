#!/bin/bash
## Set-up of variables ##

# Check that correct input arguments are provided
ENV=$1
#dbuser=$2
#dbpassword=$3

if [[ -z $ENV ]]
then
  echo "Must provide environment dev or prod as first argument"
  exit 1
fi

# Get and export required parameters: region, profile, SSH key name, bucket name, ami ID, subnet ID, tag name
profile=$(cat ./makeVPC/${ENV}.tfvars | grep 'profile' | awk -F'[=&]' '{print $2}' | tr -d '"')
chosen_region=$(cat ./makeVPC/${ENV}.tfvars | grep 'chosen_region' | awk -F'[=&]' '{print $2}' | tr -d '"')
bucket_name=$(cat ./makeVPC/${ENV}.tfvars | grep 'bucket_name' | awk -F'[=&]' '{print $2}' | tr -d '"')
amiID=$(cat ./makeVPC/${ENV}.tfvars | grep 'amiID' | awk -F'[=&]' '{print $2}' | tr -d '"')
subnetID=$(cat ./makeVPC/${ENV}.tfvars | grep 'subnetID' | awk -F'[=&]' '{print $2}' | tr -d '"')
env=$(cat ./makeVPC/${ENV}.tfvars | grep 'env' | awk -F'[=&]' '{print $2}' | tr -d '"')
ssh_key_name=$(cat ./makeVPC/${ENV}.tfvars | grep 'ssh_key_name' | awk -F'[=&]' '{print $2}' | tr -d '"')
ssh_key_path=$(cat ./makeVPC/${ENV}.tfvars | grep 'ssh_key_path' | awk -F'[=&]' '{print $2}' | tr -d '"')
tags=$(cat ./makeVPC/${ENV}.tfvars | grep 'tags' | awk -F'[=&]' '{print $2}' | tr -d '"')
dbuser=$(cat ./makeVPC/${ENV}.tfvars | grep 'dbuser' | awk -F'[=&]' '{print $2}' | tr -d '"')


# Export variable grabbed from .tfvars file
export profile chosen_region bucket_name amiID subnetID env ssh_key_name tags ssh_key_path ENV

#If argument not given raise error
if [[ -z $chosen_region ]] || [[ -z $profile ]] || [[ -z $bucket_name ]] || [[ -z $amiID ]] \
|| [[ -z $subnetID ]] || [[ -z $tags ]] || [[ -z $ssh_key_name ]] || [[ -z $env ]] || [[ -z $ssh_key_path ]]
then
  echo "Insufficient parameters in .tfvars file, please give: region, profile, SSH key name, bucket name, ami ID, subnet ID, tag name, environment"
  exit 1
fi


# Export default region
AWS_DEFAULT_REGION=$chosen_region
export AWS_DEFAULT_REGION


# Run the setenv script to export downloaded credentials
source ./setenv $profile


# Delete RDS
cd ../rds
./destroy_rds

# Delete kubernetes cluster
cd ../KCluster/K8sClusterAnsible
./destroyme

# Delete Jenkins
cd ../../Jenkins 
./destroy_jenkins.sh
