# ReadMe for Assessment 4
#### Kim, Molly, and Martha

This ReadMe file contains documentation that will assist with the use of our code.

## Contents:

* Setting up your system
    - Pre-requisites
    - Setting up python virtual environment
    - Initial variable alterations
    - SSH Keys
    - Setting up the infrastructure
    - Launching an environment
* System details
    - Core Infrastructure
    - Environment Infrastructure
    - Resetting the Database Password
    - App Development Pipeline
    - Launching a New Environment


# Setting up your system

## Pre-requisites
In order to run the following code on your machine you must:

* Log in to amazon in order to download the correct credentials.
* Follow the variable setup and instructions about SSH keys
* Activate a python virtual environment with the correct python packages installed, as detailed in the next section.

## Setting up a python virtual environment
* You need to have python virtual environements installed.
* Choose a  location to store your virtual environment, for example a directory within your home directory.
* Run ``python3 -m venv <venvName>`` to create the environment.
* Then run ``source <venvName>/bin/activate`` to activate it.
* Navigate to this project directory
* Run ``pip install -r requirements.txt`` to install all of the required libraries.

## Initial variable alterations

The following variables must be altered:

- __chosen_region__ = AWS region you wish to use
- __profile__ = AWS profile you wish to use
- __pem_file__ = name of your pem file
- __ssh_key_name__= name of your key on aws
- __bucket_name__= name you wish to call the S3 bucket
    - This bucket name MUST be all lower case and must be globally unique.
- __tags__= tag name for your infrastructure pieces
- __env__= the environment this tfvars relates to. Should be dev for the initial launch, then can be anything for further environments.
- __project_name__= name of your project
- __homeIP__= your home ip
- __db_user__= desired database username
- __dns_domain__= desired domain name
- __dns_zone_id__= id of the public hosted zone

The following variables likely do not need to be altered, but can be if you so wish:

 - __amiID__= change this to be correct for the region you are operating in if your regions is not included in the image_id mapping
 - __subnetID__= change this to be correct for the region you are operating in
 - __vpc_cidr__= Desired cidr for the VPC created by jenkins
 - __pub1_cidr__= Desired cidr for public subnet 1
 - __pub2_cidr__= Desired cidr for public subnet 2
 - __pub3_cidr__= Desired cidr for public subnet 3
 - __priv1_cidr__= Desired cidr for private subnet 1
 - __priv2_cidr__= Desired cidr for private subnet 2
 - __priv3_cidr__= Desired cidr for private subnet 3
 - __absolute_ssh_key_path__= "../"

Do not alter this variable:
 - __db_name__= petclinic

## SSH Keys

If you wish to use your own already existing SSH key, please COPY the .pem file into the assessment4 directory.

Alternatively, a new SSH key will be generated for you if the provided key name in .tfvars is invalid. This new SSH key will be named as the ssh_key_name variable is named in tfvars file within makeVPX directory, and will be downloaded to the assessment4 directory.

The valid key will then be uploaded to the keys directory in the created S3 bucket.

Once the key (.pem file) has been used by the relevant scripts, it will then be deleted from the assessment4 directory.


## Setting up the infrastructure
Before launching the infrastructure ensure you have logged into AWS so have up to date credentials.

### Initial script
Creation of the infrastructure and one environment can be done by running the command ``./initialscript.sh {env}`` where {env} is the environment you wish to deploy in. It is recommended that you set the environment to dev here as this is the base deployment.

This command will build the core infrastructure and launch your first environment, as well as:

- An S3 bucket to store system information and secrets.
- A Virtual Private Cloud which all of the infrastructure is created in to keep it private.
- A private docker registry.
- A Jenkins administration server.
- A Kubernetes cluster.
- A bastion host for SSH management access.


For more information about the individual elements, see the section on system details.


## Launching an environment
Once the infrastructure is set up you can launch as many environments as you want, each of which will have its own database and configuration of apps.

### Repeatable script

Creation of an environment can be done through running ``repeatable.sh {env}``, where {env} is the environment you wish to create. The script will:

* Create an RDS for your new environment
* Create a namespace in the kubernetes cluster
* Make a service connecting to the correct database
* Store the appropriate secrets in kubesecrets
* Make an app deployment for petclinic

The DNS of your petclinic application will depend on the variables you have cosen in the tfvars file. It will take the form ``www.petclinic.{env}.{tags}.{dns_domain}``.

### Launching more environments
Once the infrastructure is set up you can launch as many environments as you want, each of which will have its own database and configuration of apps.

You will need to make a comprehensive ENV.tfvars file for every environment you want to launch, in the makeVPC directory.
Most of the variables in this file will be exactly the same as the ones you used to launch your system, the most important one to change is env.

### Launching updated versionf of petclinic
If you wish to launch a more up to date version of petclinic which you know exists on your registry, you can also do this by running ``repeatable.sh {env} {version}`` where version is the petclinic version you wish to deploy.

This can be done even if the environment you wish to deploy into already exists.

If no version is specified petclinic 1.0, the earliest version in your registry will be deployed.

## Core infrastructure

### S3 Bucket
An S3 bucket will be launched with the name specified in the bucket_name variable in tfvars file.

This bucket is private and will contain:

* Terraform state files for each element of the infrastructure
* The ansible host file detailing information about each of the machines in the kubernets cluster
* Secrets, including
    * The SSH key used
    * Database usernames and passwords for each environment

### VPC
A virtual private cloud with three private and three public subnets, in which all of the other infrastructure will be launched.

The private subnets have access to the internet through NAT gateways.

Two security groups will be made, one with open access on all ports to other machines within this VPC, for the Kubernetes cluster and one with more restricted access, for the other machines.

### Private docker registry
A private docker registry will be created, where all docker image created will be stored. This registry is shared across environments.

### Jenkins server

A Jenkins instance will be launched with Git, Ansible, Terraform, and MysQL installed. This Jenkins machine will control Docker images, testing and deploying them.

Jenkins instance is launched within public subnet 1. Jenkins can be accessed on port 8080 of the instance, at Jenkins.(dns_domain}:8080

Jenkins username is admin, and Jenkins password is also admin.

### Kubernetes cluster

A K8s Cluster will be created through the following steps:

* Create 6 ec2 instances to host the Cluster:
    * 1 Controller Node
    * 2 Master Nodes
    * 3 Worker Nodes
* Provision the instances by installing K8s requirements
* Install K8s on the Bastion using Kubespray
    * Uses code from https://github.com/kubernetes-sigs/kubespray
* Create the Load Balancer, along with:
    * Target group
    * ELB for Public app access
    * DNS entries for Kubernetes and Applications
* Create the Namespace for the current environment:
    * dev-ns or prod-ns
* Send the daemon.json file to each master and worker node, so that the registry can be identified
* Copy K8s Config files to the Bastion


### Bastion
A loadbalanced bastion host will be created which can be used to access machines in the private subnets.
- It will have the dns name bastion.__dns_prefix__.academy.grads.al-labs.co.uk.
- The dns_prefix can be chosen in the tfvars file.


## Environment specific infrastructure

### RDS
A Mysql database server will be launched and the petclinic schema and initialisation data loaded into it automatically. The username of the database will be as specified in the tfvars file, and the password will be a default value of 'petclinic' upon database creation, but will be quickly reset to a random string. The password can be reset once the environment is built by running the changeDBpassword job on Jenkins, or by running the script ./DBpasswords/DatabaseConfig, (more information on this can be found later in the next section.

##  Resetting database password

If you wish to reset the database password to a random alphanumeric string, please run the following command:

` cd DBpasswords/DatabaseConfig {environment} {bucket_name} {ssh_key_name} {chosen_region} `

This script will first generate a new random password, before getting the database user and current password from the S3 bucket. The password for the RDS will then be changed to the new password, and connection to the database via this new password will be checked. If the password is changed successfully, the password in the S3 bucket and the Kubernetes secrets will be updated.

This script will automatically run Mon-Fri at 4:30am via a Jenkins job.

## App development pipeline
The building of images is managed automatically by the jenkins server.
The deployment of new images to each environment can be managed using jenkins jobs.

### Building images
The jenkins job __petclinic_image__ will make a docker container that runs petclinic and test it against a temporary docker container and

This job will be triggered automatically when updates are pushed to the petclinic git repository, and can also be run manually if a new image is desired.

If the tests are successful it will trigger the jenkins job __push_petclinic_to_registry__.
This job will push the new image to the private docker repository, ready to be deployed in the kubernetes clusters.

It will increment the image tag in the registry by 0.1 for every new image made.

This job will built automatically whenever a new image is made, but can also be run manually for existing images.

#### Deploy from YAML

Once images are built, the application is deployed with the following command:

`./kubectl/molly-deploy {env} {bucket_name} {ssh_key_name} {version} {ns}`,

where {env} is the desired environment, {bucket_name} is the __bucket_name__ variable, {ssh_key_name} is your __ssh_key_name__, {version} is the version of the application that you wish to run and {ns} is the namespace for the current environment.

* Before relaunching the Kubernetes Cluster ensure that KCluster/K8sCluster/environments/your_environment/hosts has been cleared of any DNS names or IPs. The file must only contain [ec2], [controller], [bastion], [kubemaster] and [kubeworker] - note that [ELB] shouldn't be included initially.
The command will take the yaml template files to create deployment, service and ingress for the application, then run them on the Kubernetes Controller. After this point there is a check to ensure that the application is available, at which the user should see an output "Petclinic deployed successfully".

## Launching a new environment
If you wish to launch a new environment/namespace, please run the command:
` ./repeatable.sh env `
where env is the name of the new environment/namespace that will be created.
This command will create a new environment with a new RDS, petclinic servers, and Kubernetes secrets.
