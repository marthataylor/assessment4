#!/bin/bash -xv
if (( $# < 4 ))
then
	echo "$0 <env> <bucket_name> <ssh_key_name> <version>"
	exit 1
fi
env=$1
bucket_name=$2
ssh_key_name=$3
version=$4
ns=$env-ns

dns_domain=$(cat ../makeVPC/${env}.tfvars | grep 'dns_domain' | awk -F'[=&]' '{print $2}' | tr -d '"')
tags=$(cat ../makeVPC/${env}.tfvars | grep 'tags' | awk -F'[=&]' '{print $2}' | tr -d '"')
dns=$env.$tags.$dns_domain

# Get registry private_ip
if aws s3 cp s3://$bucket_name/env:/dev/statefiles/makeRegistry.tfstate . >/dev/null
then
    privreg=$(cat makeRegistry.tfstate | jq -r '.outputs.registry_private_ip.value')
    echo $privreg
    rm makeRegistry.tfstate
    if [[ -z $privreg ]]
    then
        echo "Private registry server not found"
        exit 4
    fi
else
    echo "Cannot access registry state file"
    rm makeRegistry.tfstate
    exit 3
fi

#Get controller endpoint
aws s3 cp s3://$bucket_name/env:/dev/hosts hosts
controller_dns=$(cat hosts | grep kubename=controller | awk '{print($1)}')
rm hosts

#Copy up yamls and run them on the controller
sed -e "s/REGDNS/$privreg/" -e "s/VERSION/$version/" deployment.yml.tmplt >deployment.yml
scp -i ../$ssh_key_name.pem -o StrictHostKeyChecking=no deployment.yml ec2-user@$controller_dns:
scp -i ../$ssh_key_name.pem -o StrictHostKeyChecking=no app-svc.yml ec2-user@$controller_dns:
sed -e "s/DNS/$dns/" ingress.yml.tmplt >ingress.yml
scp -i ../$ssh_key_name.pem -o StrictHostKeyChecking=no ingress.yml ec2-user@$controller_dns:
ssh -i ../$ssh_key_name.pem -o StrictHostKeyChecking=no ec2-user@$controller_dns "kubectl apply -f deployment.yml -n $ns;
            kubectl apply -f app-svc.yml -n $ns;
            kubectl apply -f ingress.yml -n $ns"

rm deployment.yml ingress.yml

sleep 60

# Check deployment
if curl www.petclinic.$dns | grep petclinic
then
    echo "Petclinic deployed successfully"
else
    echo "Petclinic deployment failed"
    exit 1
fi
