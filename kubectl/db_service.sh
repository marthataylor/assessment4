#!/bin/bash +xv
if (( $# < 3 ))
then
	echo "$0 <env> <bucket_name> <ssh_key_name>"
	exit 1
fi
env=$1
bucket_name=$2
ssh_key_name=$3

#Get controller endpoint
aws s3 cp s3://$bucket_name/env:/dev/hosts hosts
controller_dns=$(cat hosts | grep kubename=controller | awk '{print($1)}')
rm hosts

#Get db endpoint
aws s3 cp s3://$bucket_name/env:/$env/statefiles/rds.tfstate rds.tfstate
DBENDPOINT=$(cat rds.tfstate | jq -r '.outputs.rds_endpoint.value')
rm rds.tfstate 

#Fill yaml template and send to controller
sed -e "s/DBENDPOINT/$DBENDPOINT/" dbconnect.yml.tmplt >dbconnect.$env.yml 
scp -i ../$ssh_key_name.pem -o StrictHostKeyChecking=no dbconnect.$env.yml ec2-user@$controller_dns:
# rm dbconnect.$env.yml

#Make service on controller
ssh -i ../$ssh_key_name.pem -o StrictHostKeyChecking=no ec2-user@$controller_dns "/usr/bin/kubectl apply -f dbconnect.$env.yml -n $env-ns"