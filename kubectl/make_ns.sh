#!/bin/bash 
if (( $# < 3 ))
then
	echo "$0 <env> <bucket_name> <ssh_key_name>"
	exit 1
fi
env=$1
bucket_name=$2
ssh_key_name=$3

#Get controller endpoint
aws s3 cp s3://$bucket_name/env:/dev/hosts hosts
controller_dns=$(cat hosts | grep kubename=controller | awk '{print($1)}')
rm hosts

if ! ssh -i ../$ssh_key_name.pem -o StrictHostKeyChecking=no ec2-user@$controller_dns "/usr/bin/kubectl get ns | grep $env-ns"
then 
    if ! ssh -i ../$ssh_key_name.pem -o StrictHostKeyChecking=no ec2-user@$controller_dns "/usr/bin/kubectl create ns $env-ns"
    then 
        echo "Namespace does not exist and creation failed"
        exit 1
    fi 
else 
    echo "Using existing namespace $env-ns"
fi
